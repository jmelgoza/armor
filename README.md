# Armor

A basic [Sinatra](http://www.sinatrarb.com/) application ready for rapid prototyping or building.

## Dependencies

[Ruby](https://www.ruby-lang.org/en/), [Sinatra](http://www.sinatrarb.com/), [Bower](http://bower.io/) and [Node.js](http://nodejs.org/)

## Get Started

### Install Ruby gems - `bundle install`

### Install Bower components - `bower install`

### Install Node.js packages - `npm install`

## Deploy on Heroku

[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## Deploy Locally

In your project's root folder run Sinatra's built-in web server and point your browser to [http://localhost:9292](http://localhost:9292)

```sh
$ bundle exec rackup
```

You can also use [Pow](http://pow.cx/)

```sh
$ cd ~/.pow && ln -s /path/to/armor_project
```

## Compile Your SASS

```sh
$ grunt style
```

Then just build.
