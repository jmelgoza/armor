# Armor v1.6

require "sinatra/base"
require "sinatra/content_for"

class App < Sinatra::Base
  helpers Sinatra::ContentFor

  # Routes

  get '/' do
    erb :'pages/index'
  end

  # Errors

  not_found do
    erb :'404'
  end

  error do
    erb :'500'
  end

  # Redirects

  get '/twitter' do
    redirect 'http://twitter.com'
  end

  # Partials

  helpers do
    def render_partial(template, args = {})
      template_array = template.to_s.split('/')
      template = template_array[0..-2].join('/') + "/_#{template_array[-1]}"
      erb(template.to_sym, :locals => args, :layout => false)
    end
  end

end
