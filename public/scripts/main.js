// main.js

require.config({
  paths: {
    jquery: '../components/jquery/dist/jquery'
  },
  shim: {
  }
});

require(['app', 'jquery'], function (app, $) {
  'use strict';
  // use app here
  console.log(app);
  console.log('Running jQuery %s', $().jquery);
});

require(['jquery'], function() {
  //some awesome code
});
